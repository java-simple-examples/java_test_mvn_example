package my.tests;

import java.util.ArrayList;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class NumericArraySimpleTest {
    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        System.out.printf("Hello and welcome! ");
        System.out.printf("%n");


        ArrayList<Integer> outputArray = createTempArray(args);

        System.out.printf("outputArray = "+outputArray.toString());
    }

    private static ArrayList<Integer> createTempArray(String[] args) {
        ArrayList<Integer> outputArray = new ArrayList<>();

        for (int i = 0; i <= args.length-1; i++) {

            Integer item = Integer.parseInt(args[i]);
            if(item % 3 == 0) {
                outputArray.add(item);
            }

        }

        return outputArray;
    }

}