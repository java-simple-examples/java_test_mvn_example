package my.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class NumericArrayStreamLambdaTest {
    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        System.out.printf("Hello and welcome! ");
        System.out.printf("%n");


        ArrayList<String> listOfString = new ArrayList<>(Arrays.asList(args));

        // Print the list of String
        System.out.printf("List of input parameters: {} ", listOfString);

        // Convert List of String to list of Integer
        List<Integer> listOfFilteredIntegers = convertStringListToIntList(
                listOfString,
                Integer::parseInt);

        // Print the list of Integer
        System.out.printf("List of parameters % 3 : {} ", listOfFilteredIntegers);
    }



    // Generic function to convert List of
    // String to List of Integer % by 3
    public static <T, U> List<U>
    convertStringListToIntList(List<T> listOfString,
                               Function<T, U> function)
    {
        return listOfString.stream()
                .map(function)
                .filter(item -> ((Integer)item) % 3 == 0)
                .collect(Collectors.toList());


    }

}