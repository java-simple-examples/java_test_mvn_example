package my.tests;

import org.apache.commons.lang3.ArrayUtils;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class EnteredNumberTest {

    public static final int TEMPLATE_NUMBER = 7;


    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.

        if (ArrayUtils.isNotEmpty(args) && Integer.parseInt(args[0]) > TEMPLATE_NUMBER) {
            System.out.printf("Hello, provided number is greater than: {} ", TEMPLATE_NUMBER);

        } else {
            System.out.printf("Hello, provided number is lower than: {} ", TEMPLATE_NUMBER);
        }


    }
}