package my.tests;

import org.apache.commons.lang3.ArrayUtils;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class EnteredNameTest {

    public static final String TEMPLATE_NAME = "Vyacheslav";

    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.

        if (ArrayUtils.isNotEmpty(args) && args[0].equalsIgnoreCase(TEMPLATE_NAME)) {
            System.out.printf("Hello, {}!", TEMPLATE_NAME);
        } else {
            System.out.printf("There is no such name");
        }


    }
}